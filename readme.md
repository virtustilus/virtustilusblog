# Virtustilus blog
## About

My first blog in Kotlin language with Spring Boot framework.

## Installation

* Install Java Development Kit 7:  Oracle JDK 7 from official website.
* Install Intellij Idea Community Edition: https://www.jetbrains.com/idea/download/ 
* Start Idea and add plugin "Kotlin" in settings.

* Clone project from bitbucket. 
* And "open" the project in Intellij Idea, screens: https://www.dropbox.com/sh/0i4q0ceexsyw49a/AAADIDNFz8cw_4H7_qzYV_YTa/spring/open_project_in_intelli_idea?dl=0
* Add maven project, kotlin library automatically (you will see it in notifications).
* Open settings of project and change project SDK to JDK 1.7.


## What next:

I'm using spring boot, whole documentation is here:
http://docs.spring.io/spring-boot/docs/current/reference/html/index.html

I'm using Kotlin language, documentation is here:
http://kotlinlang.org/docs/reference/

We can mix java and kotlin files in work.

Install dependency to local maven repository:


    mvn install:install-file -Dfile=repo/markdown4j-2.2.jar -DgroupId=org.markdown4j -DartifactId=markdown4j -Dversion=2.2 -Dpackaging=jar


## Starting:
Open `src/main/resources/application.properties` and change mysql configuration (username, password, database).    

Then right click on `src/main/java/ru/virtustilus/blog3/Blog3Application.kt` file and select `Run ...`.

Now you can try in browser: 

    http://localhost:8080/
    http://localhost:8080/clients/post
    http://localhost:8080/clients/1
    
It will ask username and password for clients route - type name `user` and password `user`
    
# Known issues

It's a problem with GeneratedValue type for Hibernate repository. 
Temporarily moved code to java file.  
Trying to find a solution.

# What to do next:

1. In show page route, add `<pre>` tags to show data of page in the same
manner as saved in database.
1. Add route that returns sorted page names with sortId value.
1. Move articles to own page with route like /static/{pageSlug}, 
and we will be able to open it by the link from homepage. 
1. Add flag showOnHome to PageEntity. 
(when enabled, full html will be shown, instead of title only)  
1. Add functionality for commenting.
1. For admin: ability to see the list of pages, edit them, delete, add new. 
 



