#!/bin/bash

# file should contain, for example: www-data@53.221.34.44
USER_SERVER=`cat ./user_server`

#file should contain path, for example: /var/www/blog3
PROJECT_PATH=`cat ./path_on_server`

echo "----Copying data-----"
rsync -azv ./target/blog3-0.0.1-SNAPSHOT.jar ${USER_SERVER}:${PROJECT_PATH}/blog3-0.0.1-SNAPSHOT.jar
rsync -azv ./restartBlog.sh ${USER_SERVER}:${PROJECT_PATH}/restartBlog.sh
rsync -azv ./application-prod.properties ${USER_SERVER}:${PROJECT_PATH}/application.properties
rsync -azv ./path_on_server ${USER_SERVER}:${PROJECT_PATH}/
rsync -azv ./proxy_config ${USER_SERVER}:${PROJECT_PATH}/
rsync -azv ./test.php ${USER_SERVER}:${PROJECT_PATH}/

echo "----Restarting server-----"
ssh ${USER_SERVER} "sh -c 'cd ${PROJECT_PATH}; nohup ${PROJECT_PATH}/restartBlog.sh >${PROJECT_PATH}/restartBlog.log 2>&1 &'; echo 'deployment can take about 5 minutes more'; sleep 5; tail -f ${PROJECT_PATH}/restartBlog.log;"
