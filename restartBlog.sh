#!/bin/bash

#file should contain path, for example: /var/www/blog3
PROJECT_PATH=`cat ./path_on_server`

#file should contain path to apache or nginx config, for example: /etc/apache2/sites-enabled/blog3
PROXY_CONFIG=`cat ./proxy_config`

OLD_PROCESS_ID=`ps ax | grep "[b]log3-0.0.1-SNAPSHOT.jar" | perl -pe "s/\s*(\d+)\s*.*/\1/g"`
echo "OLD Process Id: ${OLD_PROCESS_ID}"

PORT=`cat ~/port`
if [ -z "$PORT" ]; then
    PORT="8081"
fi
if [ "$PORT" -gt "8090" ]; then
    PORT="8081"
fi
if [ "$PORT" -lt "8081" ]; then
    PORT="8081"
fi

let "PORT=PORT+1"
echo "New PORT number $PORT"
echo "$PORT" > ~/port

UPDATEDAT=`date +"%Y-%m-%d %H-%M-%S"`

echo "Starting new"
sh -c "cd ${PROJECT_PATH}/; nohup java -jar blog3-0.0.1-SNAPSHOT.jar --blog_updated_at=\"$UPDATEDAT\" --server_port=\"$PORT\" > ${PROJECT_PATH}/spring.log &"

echo "Waiting for new instance up"
CURRENT_TIME=`date +%s`
let "STOP_TIME = CURRENT_TIME + 1200"
STATUS=7
while [ "$STATUS" -ne "0" ]; do
    curl http://localhost:${PORT}/ 2>/dev/null >/dev/null
    STATUS="$?"
    CURRENT_TIME=`date +%s`
    let "DIFFERENCE = STOP_TIME - CURRENT_TIME"
    if [ "$DIFFERENCE" -lt "1" ]; then
        echo "!!! TIMEOUT !!!"
        exit 7
    fi
    sleep 5
    TIME=`date +"%Y-%m-%d %H-%M-%S"`
    echo "$DIFFERENCE ($TIME)"
done

echo "Started, changing configuration"

### Reconfigure apache to new server
sudo perl -i -p -e "s/127.0.0.1:\d+/127.0.0.1:${PORT}/g" ${PROXY_CONFIG}

### Now change to new one
sudo service nginx restart

echo "Waiting 10 seconds before killing old process"
sleep 10

if [ ! -z ${OLD_PROCESS_ID} ]; then
    echo "Stop old process"
    sudo sh -c "kill ${OLD_PROCESS_ID}"
fi

echo "Done"