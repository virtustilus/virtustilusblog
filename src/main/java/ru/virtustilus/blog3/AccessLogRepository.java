package ru.virtustilus.blog3;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AccessLogRepository extends CrudRepository<AccessLogEntity, Long> {
    List<PageEntity> findByHost(String name);
}