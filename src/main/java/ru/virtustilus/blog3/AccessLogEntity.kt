package ru.virtustilus.blog3

import java.util.Date
import javax.persistence.*

@NamedQueries(
        NamedQuery(
                name = "HQL_GET_ALL_BY_DAY",
                query = """select host, count(id) as c
                        from AccessLogEntity a
                        where a.createdAt >= :startDate and a.createdAt < :endDate
                        group by host"""
        )
)
@Entity
@Table(name = "access_log",
        indexes = arrayOf(Index(columnList = "createdAt", name = "created_at_idx"),
        Index(columnList = "host", name = "host_idx")
))
data class AccessLogEntity(
        @Id
        @GeneratedValue
        var id: Long = 0,
        @Column
        var host: String = "",
        @Column
        var ip: String = "",
        @Column
        var createdAt: Date = Date()
)