package ru.virtustilus.blog3

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity

@EnableWebSecurity
@SpringBootApplication
open public class Blog3Application

public fun main(args: Array<String>) {
    SpringApplication.run(javaClass<Blog3Application>(), *args)
}

