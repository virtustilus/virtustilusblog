package ru.virtustilus.blog3.Controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import org.springframework.ui.ModelMap
import ru.virtustilus.blog3.AccessLogEntity
import ru.virtustilus.blog3.AccessLogRepository
import ru.virtustilus.blog3.MetaVerification
import ru.virtustilus.blog3.PageRepository
import java.util.regex.Pattern
import javax.servlet.http.HttpServletRequest

@Controller
class HelloController {

    @Autowired
    var pageRepository: PageRepository? = null

    @Autowired
    var accessLogRepository: AccessLogRepository? = null

    @Value("\${blog3.verifications}")
    var verificationsString: String? = null

    @Value("\${blog3.updated_at}")
    var updatedAt: String? = null

    @RequestMapping(value = "/", method = arrayOf(RequestMethod.GET))
    public fun indexAction(@RequestParam(value = "name", required = false, defaultValue = "Eva") name: String, model: ModelMap, request: HttpServletRequest): String {

        var pages = pageRepository?.findAll()
        pages = pages?.sortedBy { it.getSortColumn() }

        model.addAttribute("name", "Virtustilus")
        model.addAttribute("pages", pages)

        val verifications = arrayListOf<MetaVerification>()
        val systems = verificationsString?.let { it.split(",".toRegex()).toTypedArray() }
        if (systems != null) {
            for (s in systems) {
                val parts = s.split("=".toRegex()).toTypedArray()
                verifications.add(MetaVerification(parts[0], parts[1]))
            }
        }

        model.addAttribute("verifications", verifications)
        model.addAttribute("updatedAt", updatedAt)

        val accessLogEntity = AccessLogEntity()
        val referrer = request.getHeader("referer")
        if (referrer != null) {
            val p = Pattern.compile("^https?://([^/]+)/.*$")
            val matcher = p.matcher(referrer)
            if (matcher.matches()) {
                accessLogEntity.host = matcher.group(1)
            }
        }

        val ip = request.getRemoteAddr()
        if (ip != null) {
            accessLogEntity.ip = ip
        }

        accessLogRepository?.save(accessLogEntity)
        model.addAttribute("maxId", accessLogEntity.id)

        return "index";
    }
}