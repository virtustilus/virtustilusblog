package ru.virtustilus.blog3.Controller

import org.hibernate.Session
import org.hibernate.SessionFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.ResourceNotFoundException
import org.springframework.security.web.csrf.CsrfToken
import org.springframework.security.web.csrf.CsrfTokenRepository
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*
import ru.virtustilus.blog3.AccessLogRepository
import ru.virtustilus.blog3.FormType.PageFormType
import ru.virtustilus.blog3.HostCount
import ru.virtustilus.blog3.PageEntity
import ru.virtustilus.blog3.PageRepository
import java.util.Calendar
import java.util.Date
import java.util.GregorianCalendar
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.PersistenceContext
import javax.servlet.http.HttpServletRequest
import javax.validation.Valid

@Controller
class PageController {

    @Autowired
    var pageRepository: PageRepository? = null

    @PersistenceContext
    var entityManager: EntityManager? = null;

    @RequestMapping(value = "/pages/{id}", method = arrayOf(RequestMethod.GET))
    public fun getPageAction(@PathVariable id: String, model: ModelMap): String {

        val idInt = id.toLong()
        val pageEntity = pageRepository?.findOne(idInt) ?: throw ResourceNotFoundException()

        model.addAttribute("pageEntity", pageEntity)

        return "page/show"
    }

    @RequestMapping(value = "/pages/new", method = arrayOf(RequestMethod.GET))
    public fun getNewPageAction(request: HttpServletRequest, model: ModelMap): String {

        val token = request.getAttribute("_csrf")
        if (token is CsrfToken) {
            model.addAttribute("_csrf", token)
        }
        return "page/new"
    }

    @RequestMapping(value = "/pages", method = arrayOf(RequestMethod.POST))
    public fun postPageAction(@ModelAttribute("form") @Valid form: PageFormType, result: BindingResult): String {

        if (result.hasErrors()) {
            return "page/new"
        }
        val pageEntity = PageEntity()
        pageEntity.name = form.name
        pageEntity.pageData = form.data
        pageEntity.title = form.title
        pageEntity.slug = form.title.replace("\\W".toRegex(), "_").toLowerCase()
        pageEntity.sortId = form.sortId
        pageRepository?.save(pageEntity)

        return "redirect:/pages/" + pageEntity.id.toString()
    }

    @RequestMapping(value = "/pages/{id}/delete", method = arrayOf(RequestMethod.GET))
    public fun deletePageAction(@PathVariable id: String): String {

        val idInt = id.toLong()
        val pageEntity = pageRepository?.findOne(idInt)

        if (pageEntity != null) {
            pageRepository?.delete(pageEntity);
            return "redirect:/"
        }

        throw ResourceNotFoundException()
    }

    @RequestMapping(value = "/statistic", method = arrayOf(RequestMethod.GET))
    public fun statisticAction(model: ModelMap): String {

        val session = entityManager?.unwrap(javaClass<Session>())

        val startDate = GregorianCalendar()
        startDate.add(Calendar.DAY_OF_MONTH, -7)

        val endDate = GregorianCalendar()
        endDate.add(Calendar.DAY_OF_MONTH, 1)

        val query = session?.getNamedQuery("HQL_GET_ALL_BY_DAY")
        query?.setDate("startDate", startDate.getTime())
        query?.setDate("endDate", endDate.getTime())
        val addressObjArray = query?.list();

        var hosts = arrayListOf<HostCount>()
        if (addressObjArray != null) {
            for (row in addressObjArray) {

                if (row is Array<*>) {
                    val o1 = row.get(0) as String;
                    val o2 = row.get(1) as Long;

                    hosts.add(HostCount(o1, o2.toInt()))
                }
            }
        }

        model.addAttribute("startDate", startDate.getTime())
        model.addAttribute("endDate", endDate.getTime())
        model.addAttribute("hosts", hosts)

        return "statistic"
    }
}
