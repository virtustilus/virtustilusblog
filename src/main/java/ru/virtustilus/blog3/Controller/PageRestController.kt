package ru.virtustilus.blog3.Controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.rest.webmvc.ResourceNotFoundException
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.*
import ru.virtustilus.blog3.EmptyJsonResponse
import ru.virtustilus.blog3.PageEntity
import ru.virtustilus.blog3.PageRepository

@RestController
@RequestMapping(value = "/api")
class PageRestController {

    @Autowired
    var pageRepository: PageRepository? = null

    @RequestMapping(value = "/pages/{id}", method = arrayOf(RequestMethod.GET))
    @ResponseBody
    public fun getPageAction(@PathVariable id: String): PageEntity {

        val idInt = id.toLong()
        val pageEntity = pageRepository?.findOne(idInt)

        if (pageEntity == null) {
            throw ResourceNotFoundException()
        }

        return pageEntity
    }

    @RequestMapping(value = "/pages", method = arrayOf(RequestMethod.POST))
    @ResponseBody
    public fun postPageAction(@RequestParam allRequestParams: Map<String, String>): EmptyJsonResponse {

        val pageEntity = PageEntity()
        pageEntity.name = allRequestParams["name"].toString()
        pageEntity.title = allRequestParams["title"].toString()
        pageEntity.pageData = allRequestParams["pageData"].toString()
        pageEntity.slug = allRequestParams["slug"].toString()
        pageEntity.sortId = allRequestParams["sortId"]?.toInt() ?: 100

        pageRepository?.save(pageEntity)

        return EmptyJsonResponse()
    }
}