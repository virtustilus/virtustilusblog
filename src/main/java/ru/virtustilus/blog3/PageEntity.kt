package ru.virtustilus.blog3

import org.markdown4j.Markdown4jProcessor
import java.io.Serializable
import java.util.Date
import javax.persistence.*

@Entity
@Table(name = "page")
data class PageEntity(

        @Id
        @GeneratedValue
        val id: Long = 0,

        @Column(name = "name")
        var name: String = "",

        @Column(name = "title")
        var title: String = "",

        @Column(name = "data", columnDefinition = "TEXT")
        var pageData: String = "",

        @Column(name = "slug", length = 1024)
        var slug: String = "",

        @Column(name = "updatedAt")
        var updatedAt: Date = Date(),

        @Column(name = "sortId")
        var sortId: Int? = null

) {

    public fun getSortColumn(): Int {
        return sortId ?: 100
    }

    public fun getHtmlView(): String {
        val processor = Markdown4jProcessor()
        return processor.process(pageData)
    }
}