package ru.virtustilus.blog3

data class HostCount(
        val host: String = "",
        val count: Int = 0
)