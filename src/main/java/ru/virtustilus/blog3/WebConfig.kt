package ru.virtustilus.blog3

import com.lyncode.jtwig.mvc.JtwigViewResolver
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.ViewResolver

@Configuration
open public class WebConfig {

    @Bean
    open public fun viewResolver(): ViewResolver {
        val viewResolver = JtwigViewResolver()
        viewResolver.setPrefix("classpath:/templates/")
        viewResolver.setSuffix(".twig")
        return viewResolver
    }
}