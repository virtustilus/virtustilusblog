package ru.virtustilus.blog3;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PageRepository extends CrudRepository<PageEntity, Long> {
    List<PageEntity> findByName(String name);
}