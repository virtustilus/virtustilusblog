package ru.virtustilus.blog3

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@EnableWebSecurity
@Configuration
open class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        val kotlinProblem = http
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/resources/**", "/signup", "/about").permitAll()
                .anyRequest().authenticated()
                .and()
        if (kotlinProblem !is HttpSecurity) {
            return
        }
        kotlinProblem.httpBasic()
    }
}