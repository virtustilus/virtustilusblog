package ru.virtustilus.blog3.FormType

import org.hibernate.validator.constraints.NotEmpty
import javax.validation.constraints.Size

data class PageFormType(

        @NotEmpty
        @Size(max = 255)
        var name: String = "",
        @NotEmpty
        @Size(max = 255)
        var title: String = "",
        var data: String = "",
        var sortId: Int = 100
)