package ru.virtustilus.blog3;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import org.markdown4j.Markdown4jProcessor;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import ru.virtustilus.blog3.Blog3Application;

import java.io.IOException;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Blog3Application.class)
@WebAppConfiguration
public class Blog3ApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void mardownAccessible() {
        Markdown4jProcessor mm = new Markdown4jProcessor();
        assertThat(mm, isA(Markdown4jProcessor.class));
        String s = "";
        try {
            s = mm.process("### Ok");
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertThat(s, not(""));
        assertThat(s, notNullValue());
    }

}
